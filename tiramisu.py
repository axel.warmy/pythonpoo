class Tiramisu:
    def __init__(self, sucre_roux, sucre_vanille, biscuits, oeufs, mascarpone, cafe, cacao):
        self.set_sucre_roux(sucre_roux)
        self.set_sucre_vanille(sucre_vanille)
        self.set_biscuits(biscuits)
        self.set_oeufs(oeufs)
        self.set_mascarpone(mascarpone)
        self.set_cafe(cafe)
        self.set_cacao(cacao)

    def get_sucre_roux(self):
        return self._sucre_roux

    def set_sucre_roux(self, sucre_roux):
        self._sucre_roux = sucre_roux

    def get_sucre_vanille(self):
        return self._sucre_vanille

    def set_sucre_vanille(self, sucre_vanille):
        self._sucre_vanille = sucre_vanille

    def get_biscuits(self):
        return self._biscuits

    def set_biscuits(self, biscuits):
        self._biscuits = biscuits

    def get_oeufs(self):
        return self._oeufs

    def set_oeufs(self, oeufs):
        self._oeufs = oeufs

    def get_mascarpone(self):
        return self._mascarpone

    def set_mascarpone(self, mascarpone):
        self._mascarpone = mascarpone

    def get_cafe(self):
        return self._cafe

    def set_cafe(self, cafe):
        self._cafe = cafe

    def get_cacao(self):
        return self._cacao

    def set_cacao(self, cacao):
        self._cacao = cacao

    def annoncer_ingrédients(self):
        print(
            f"Pour la recette il faut : {self.get_sucre_roux()} de sucre roux,\n {self.get_sucre_vanille()} de sucre vanille,\n {self.get_biscuits()} biscuits,\n {self.get_oeufs()} oeufs,\n {self.get_mascarpone()} de mascarpone,\n {self.get_cafe()} de café,\n {self.get_cacao()} de cacao,\n et voici les étapes de la recette :")
    
    def etapes(self):
        print(f"Séparer les blancs des jaunes des {self.get_oeufs()} oeufs.\n")
        print(f"Mélanger les jaunes avec les {self.get_sucre_roux()} de sucre roux et les {self.get_sucre_vanille()} de sucre vanillé.\n ")
        print(f"Ajouter les {self.get_mascarpone()} de mascarpone au fouet.\n")
        print(f"Monter les blancs en neige et les incorporer délicatement à la spatule au mélange précédent. Réserver.\n")
        print(f"Mouiller les {self.get_biscuits()} biscuits dans les {self.get_cafe()} de café rapidement avant d'en tapisser le fond du plat.\n")
        print(f"Recouvrir d'une couche de crème les {self.get_mascarpone()} de mascarpone puis répéter l'opération en alternant couche de biscuits et couche de crème en terminant par cette dernière.\n")
        print(f"Saupoudrer de {self.get_cacao()} de cacao.\n")
        print(f"Mettre au réfrigérateur 4 heures minimum puis déguster frais.\n")

ingredients = Tiramisu("100g","1 sachet",24,3,"250g","50cl","30g")
ingredients.annoncer_ingrédients()
ingredients.etapes()