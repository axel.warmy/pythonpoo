class Rectangle:
    def __init__(self, a, b) :
        self.set_a(a)
        self.set_b(b)


    def get_a(self):
        return self._a

    def set_a(self, a):
        self._a = a

    
    def get_b(self):
        return self._b

    def set_b(self, b):
        self._b = b

    
    def surface(self):
        return self._a * self._b

rectangle = Rectangle(2,7)
print(rectangle.surface())
