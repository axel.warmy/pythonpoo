class Complex:
    def __init__(self, reel1, imaginaire1, reel2, imaginaire2) :
        self.set_reel1(reel1)
        self.set_imaginaire1(imaginaire1)
        self.set_reel2(reel2)
        self.set_imaginaire2(imaginaire2)

    def get_reel1(self):
        return self._reel1

    def get_imaginaire1(self):
        return self._imaginaire1

    def get_reel2(self):
        return self._reel2

    def get_imaginaire2(self):
        return self._imaginaire2

    def set_reel1(self, reel1):
        self._reel1 = reel1

    def set_imaginaire1(self, imaginaire1):
        self._imaginaire1 = imaginaire1

    def set_reel2(self, reel2):
        self._reel2 = reel2

    def set_imaginaire2(self, imaginaire2):
        self._imaginaire2 = imaginaire2

    def show_complex(self):
        return str((int(self._reel1) + int(self._reel2))) + " + " + str((int(self._imaginaire1) + int(self._imaginaire2))) + "i"

