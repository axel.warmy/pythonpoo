from pokemon import Pokemon


class Fire(Pokemon):
    def __init__(self, name, hp, atk):
        super().__init__(name, hp, atk)

    def get_type(self):
        return "fire"
