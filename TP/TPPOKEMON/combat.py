import random
import time

class Combat:
    def __init__(self, p1, p2):
        self._p1 = p1
        self._p2 = p2
        self.efficacite()
        self.combat()

    def efficacite(self):
        self._eff = 1
        if self._p1.get_type() == "fire" and self._p2.get_type() == "water":
            self._eff = 0.5
        elif self._p1.get_type() == "fire" and self._p2.get_type() == "grass":
            self._eff = 2
        elif self._p1.get_type() == "grass" and self._p2.get_type() == "water":
            self._eff = 2
        elif self._p1.get_type() == "grass" and self._p2.get_type() == "fire":
            self._eff = 0.5
        elif self._p1.get_type() == "water" and self._p2.get_type() == "grass":
            self._eff = 0.5
        elif self._p1.get_type() == "water" and self._p2.get_type() == "fire":
            self._eff = 2
        return self._eff

    def combat(self):
        while self.isFighting():
            self.start_msg()
            if self.crit() == True :
                self.crit_msg()
                self._p2.set_hp(self._p2.get_hp() - self._p1.get_atk() * self.efficacite() * 2)
                self.pause()
            elif self.miss() == True :
                self.miss_msg()
                self._p2.set_hp(self._p2.get_hp() - self._p1.get_atk() * self.efficacite() * 0)
                self.pause()
            else :
                self.base_msg()
                self._p2.set_hp(self._p2.get_hp() - self._p1.get_atk() * self.efficacite())
                self.pause()
            self.switch_p()

    def isFighting(self):
        return self._p1.isDead() == False and self._p2.isDead() == False

    def switch_p(self):
        self._p1, self._p2 = self._p2, self._p1

    def crit(self):
        critique = False
        i = random.randint(0,100)
        if i <= 15:
            critique = True
        return critique

    def miss(self):
        rate = False
        i = random.randint(0,10)
        if i == 0:
            rate = True
        return rate

    def base_msg(self):
        print("..\n")
        self.pause()
        print(f"Il lui inflige {self._p1.get_atk() * self.efficacite()} dégats !!\nIl ne reste que {self._p2.get_hp() - self._p1.get_atk() * self.efficacite()} HP à {self._p2.get_nom()}\n ")
        self.pause()

    def crit_msg(self):
        print("..\n")
        self.pause()
        print("Coup critique !!")
        self.pause()
        print(f"Il lui inflige {self._p1.get_atk() * self.efficacite() * 2} dégats !!\nIl ne reste que {self._p2.get_hp() - self._p1.get_atk() * self.efficacite() * 2} HP à {self._p2.get_nom()}\n")
        self.pause()

    def miss_msg(self):
        print("..\n")
        self.pause()
        print(f"Mais cela échoue.\nIl reste {self._p2.get_hp() - self._p1.get_atk() * self.efficacite() * 0} HP à {self._p2.get_nom()}\n ")
        self.pause()
        
    def start_msg(self):
        print(f"{self._p1.get_nom()} attaque {self._p2.get_nom()}.\n")
        self.pause()

    def pause(self):
        time.sleep(1.5)