class Pokemon:
    def __init__(self, nom, hp, atk):
        self.set_nom(nom)
        self.set_hp(hp)
        self.set_atk(atk)

    def get_nom(self):
        return self._nom

    def get_hp(self):
        return self._hp

    def get_atk(self):
        return self._atk

    def set_nom(self, nom):
        self._nom = nom

    def set_hp(self, hp):
        self._hp = hp

    def set_atk(self, atk):
        self._atk = atk

    def isDead(self):
        isDead = False
        if int(self._hp) <= 0:
            print(f"{self._nom} est hors combat. L'adversaire gagne le match")
            isDead = True
        return isDead

    def toString(self):
        print(
            f" voici les caractéristiques du Pokémon : {self._nom} {self._hp} {self._atk}")
