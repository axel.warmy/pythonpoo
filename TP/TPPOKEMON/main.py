from combat import Combat
from fire import Fire
from grass import Grass
from water import Water


salameche = Fire("Salameche", 50, 10)
bulbizarre = Grass("Bulbizarre", 50, 250)
carapuce = Water("Carapuce", 50, 10)
Combat(salameche, carapuce)
