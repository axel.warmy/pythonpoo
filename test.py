class Test:

    def __init__(self,nom) :
        self.set_nom(nom)

    def set_nom(self,nom):
        self._nom = nom

    def get_nom(self):
        return self._nom

    def get_prenom(self):
        return self._prenom

    def message(self):
        print(f"il s'appelle {self.get_nom()} {self.get_prenom()}")

instance = Test("Buffet")
print(instance.get_nom())
instance._prenom = "Ashton"
print(instance.get_prenom())
instance.message()

'''
Instancier une classe -> Créer un objet 
self -> référence à l'objet en cours 
les attributs sont dynamiques -> crées de manières dynamiques, quand on en a besoin
sette -> définir un attribut en cocordance avec l'état attentdu ( age > 0 et < 150 par exemple)
gette -> afficher l'attribut 
attribut -> variable de classe
méthode -> fonction de classe
'''