class Voiture:
    def __init__(self, carburant, couleur):
        self.set_couleur(couleur)
        self.set_carburant(carburant)

    def get_couleur(self):
        return self._couleur
    
    def set_couleur(self,couleur):
        if (type(couleur) == str):
            self._couleur = couleur

    def get_carburant(self):
        return self._carburant

    def set_carburant(self,carburant):
        if (type(carburant) == str) :
            self._carburant = carburant

    def avancer(self):
        return"La voiture avance"

    def freiner(self):
        return"La voiture freine"

    def message(self):
        print(self.avancer())
        print(self.freiner())

v1 = Voiture("Diesel","verte")
v1.message()
print(v1.get_couleur(),v1.get_carburant())
print(v1.__dict__)