from animal import Animal


class Chien(Animal):
    def __init__(self, race):
        super().__init__(race)

    def message(self):
        return "classe chien"

chien = Chien("Malinois")
print(chien.get_race())
print(chien.nom)
print(chien.message())