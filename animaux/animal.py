class Animal:
    def __init__(self, race):
        self.set_race(race)
        self.nom = "Rex"

    def set_race(self, race):
        self.__race = race

    def get_race(self):
        return self.__race

    def message():
        return "classe animale"