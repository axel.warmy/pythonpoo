from animal import Animal


class Chat(Animal):
    def __init__(self, race):
        super().__init__(race)

chat = Chat("Maicoon")
print(chat.get_race())